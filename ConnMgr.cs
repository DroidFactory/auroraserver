using System;
using System.Collections.Generic;
using System.Text;
using NetworkTube;

namespace Projekt_server
{
    
    public class ConnMgr : DbMother<Connection>
    {
        public static Connection _GetCon(NTube tube)
        {
            foreach(Connection c in ConnMgr._db)
                if (c.tube == tube)
                    return c;
            return null;
        }

        public static Connection _GetCon(User usr)
        {
            foreach (Connection c in ConnMgr._db)
                if (c.usr == usr)
                    return c;
            return null;
        }
    }


    public class Connection
    {
        public NTube tube;
        public User usr;
        public Connection()
        { }
    }
}

