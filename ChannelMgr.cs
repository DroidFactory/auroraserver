using System;
using System.Collections.Generic;
using System.Text;

namespace Projekt_server
{
    public class ChannelMgr : DbMother<Channel>
    {
        public static List<Channel> _GetChanList(User usr)
        {
            List<Channel> ret = new List<Channel>();
            foreach (Channel c in ChannelMgr._db)
            {
                foreach (User u in c.usrList)
                {
                    if (u == usr)
                        ret.Add(c);
                }
            }
            return ret;
        }

        public static Channel _GetChan(string chan_name)
        {
            foreach (Channel c in ChannelMgr._db)
            {
                if (c.chan_name == chan_name)
                    return c;
            }
            return null;
        }

        public static void _CreateChan(string name)
        {
            Channel chan = new Channel();
            chan.chan_name = name;
            _db.Add(chan);
        }

        public static bool _Exists(string chan_name)
        {
            foreach (Channel c in ChannelMgr._db)
            {
                if (c.chan_name == chan_name)
                    return true;
            }
            return false;
        }
    }

    public class Channel
    {
        public List<User> usrList = new List<User>();
        public string chan_name;
        public Channel()
        { }
    }
}
