using System;
using System.Collections.Generic;
using System.Text;

namespace Projekt_server
{
    public class UserMgr : DbMother<User>
    {
        public static User _Search(string username)
        {
            foreach (User u in UserMgr._db)
            {
                if (u.username == username)
                    return u;
            }
            return null;
        }
    }

    public class User
    {
        public string username;
        public string full_name;
        public string email;
        public string comment;
        public User()
        { }
    }
}
