using System;
using System.Collections.Generic;
using System.Text;
using NetworkTube;

namespace Projekt_server
{
    public static class Stuff
    {
        public static NTube listener = new NTube("Listener", "Listens for incoming connections");
        public static string serverName = "deckmars testserver";
        public static string motd = "Message of the day: Snart/nu �r det redovisning";
        public enum debuglevel { dirty_debugging = 1, normal_debuging, normal, error, off };
        public static debuglevel debug_output = debuglevel.normal;

        // Timestamp
        public static string timestamp
        { get { return "[" + DateTime.Now.ToLongTimeString() + "]"; } }

        // Say (into the console)
        public static void say(string msg, bool newline)
        {
            Console.Write(" " + Stuff.timestamp + " " + msg + ((newline) ? "\n" : ""));
        }
        public static void say(string msg)
        { say(msg, true); }

        public static void debug(string msg, debuglevel lvl)
        {
            if (lvl >= Stuff.debug_output)
                say("[dbg " + lvl.ToString() + "] " + msg); 
        }
        public static void debug(string msg)
        { debug(msg, debuglevel.normal); }

        public static bool nickExists(string nick)
        {
            User test = UserMgr._Search(nick);
            if (test == null) return false;
            else return true;
        }

        // Some shortcuts..
        public static void send_YouShallNotPass(NTube tube, string title, string msg)
        {
            YouShallNotPass sry = new YouShallNotPass();
            sry.title = title;
            sry.msg = msg;
            tube.send_object<YouShallNotPass>(sry);
        }

        public static void send_Welcome(NTube tube, string msg)
        {
            Welcome welc = new Welcome();
            welc.srvName = Stuff.serverName;
            welc.msg = msg;
            welc.motd = Stuff.motd;
            tube.send_object<Welcome>(welc);
        }

        public static List<User> GetAllUsersInTheSameChannelsAs(User usr)
        {
            List<User> ret = new List<User>();
            List<Channel> chans = ChannelMgr._GetChanList(usr);
            foreach (Channel c in chans)
            {
                foreach (User u in c.usrList)
                {
                    bool add = true;
                    foreach (User u1 in ret)
                        if (u.username == u1.username) add = false;
                    if (add)
                        ret.Add(u);
                }
            }
            return ret;
        }
    }


    // Functions for listener and its offspring
    partial class Program
    {
        static void srv_incomming_connection(NTube tube, IAsyncResult iasync)
        {
            // When user connects - add it to incoming objects and wait for "HiThere"-message
            NTube newTube = new NTube(tube.get_connecting_socket(iasync));
            Stuff.say("[Con] Fick anslutning fr�n " + newTube.remoteIP);
            newTube.event_incoming_object += new NTube.dele_incoming_object(srv_incomming_object);
            newTube.event_tube_disconnected += new NTube.dele_tube_disconnected(srv_disconnected);
            newTube.start_async_read();
            tube.start_async_accept();
        }


        static void srv_disconnected(NTube tube)
        {
            Connection con = ConnMgr._GetCon(tube);
            if (con != null)
            {
                Stuff.say("[Con] Anslutningen till \"" + con.usr.username + "\" (" + tube.remoteIP + ") st�ngdes");
                foreach (Channel ch in ChannelMgr._GetChanList(con.usr))
                {
                    Control c = new Control();
                    c.control_type = "Quit";
                    c.args.Add(ch.chan_name);
                    c.args.Add(con.usr.username);
                    foreach (User u in ch.usrList)
                    {
                        if (u.username == con.usr.username) continue;
                        ConnMgr._GetCon(u).tube.send_object<Control>(c);
                    }
                    ch.usrList.Remove(con.usr);
                }
                UserMgr._Remove(con.usr);
                ConnMgr._Remove(con);
            }
            else
            {
                Stuff.say("[Con] Anslutningen till (" + tube.remoteIP + ") st�ngdes");
            }
        }

        static void srv_incomming_object(string obj_name, string obj_xml, NTube tube)
        {
            switch (obj_name)
            {
                case "HiThere":
                    Inc_HiThere(NTube.get_object<HiThere>(obj_xml), tube);
                    break;

                case "Control":
                    Inc_Control(NTube.get_object<Control>(obj_xml), tube);                    
                    break;

                case "Message":
                    Inc_Message(NTube.get_object<Message>(obj_xml), tube);
                    break;
            }
        }
    }
}
