using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Projekt_server
{
    public class HiThere
    {
        public User user;
        public string version;
        public HiThere()
        { }
    }

    public class Welcome
    {
        public string msg;
        public string srvName;
        public string motd;
        public Welcome()
        { }
    }

    public class YouShallNotPass
    {
        public string title;
        public string msg;
        public YouShallNotPass()
        { }
    }

    public class Message
    {
        public string msg_type;
        public string msg;
        public string from;
        public string target;
        public SimpleFont font;

        public Message()
        { }
    }

    public class Control
    {
        public string control_type;
        public List<string> args = new List<string>();
        public Control()
        { }
    }



    public struct SimpleFont
    {
        // The class Font is too complex to serialize, so we replace it with this
        // "SimpleFont" witch takes out the important parts and can convert SimpleFont <--> Font
        public string FontFamily;
        public GraphicsUnit GraphicsUnit;
        public float Size;
        public FontStyle Style;

        public SimpleFont(Font fo)
        {
            FontFamily = fo.FontFamily.Name;
            GraphicsUnit = fo.Unit;
            Size = fo.Size;
            Style = fo.Style;
        }

        public Font ToFont() { return new Font(FontFamily, Size, Style, GraphicsUnit); }
    }
}
