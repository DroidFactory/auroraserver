using System;
using System.Collections.Generic;
using System.Text;

namespace Projekt_server
{
    public class DbMother<T> where T : class
    {
        public static List<T> _db = new List<T>();

        public static void _Add(T me)
        {
            _db.Add(me);
        }

        public static void _Remove(T me)
        {
            _db.Remove(me);
        }

    }
}
