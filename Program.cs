using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using NetworkTube;
using System.Threading;
using System.Net.Sockets;

namespace Projekt_server
{
    public partial class Program
    {
        static void Main(string[] args)
        {
            //Console.BackgroundColor = ConsoleColor.Gray;
            //Console.ForegroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            //Console.WindowWidth = 120;
            //Console.WindowHeight = 80;
            Console.Title = "Projektarbete -- Server";
            Console.CursorVisible = false;
            //Console.ForegroundColor = ConsoleColor.White;

            int listen_port = 6262;
            /*
             * Det h�r �r servern till chatt och spel
             * 
             * By the way .. Networking.cs (NTube-klassen) �r med i det h�r projektet.
             * Det �r samma fil som klient-projektet, inte bara en kopia.
             */
            Stuff.debug_output = Stuff.debuglevel.normal_debuging;// .dirty_debugging;

            Stuff.debug("Skapar server-tuben", Stuff.debuglevel.normal_debuging);
            Stuff.listener.event_incoming_connection += new NTube.dele_incoming_connection(srv_incomming_connection);
            Stuff.debug("Lyssnar p� port " + listen_port.ToString(), Stuff.debuglevel.normal);
            Stuff.listener.listen_and_accept(listen_port);

            Stuff.debug("[Sys] Servern lyssnar nu p� port 6262..");
            Stuff.debug("[Sys] Pingar alla anslutningar ca. var 20:e sekund..");
            while (true)
            {
                Stuff.debug("Sover i 20 sec", Stuff.debuglevel.dirty_debugging);
                Thread.Sleep(20 * 1000);  // Sleep 20 sec
                foreach (Connection con in ConnMgr._db)
                {
                    if (con.tube.Connected != true)
                    {
                        Stuff.debug("F�rs�kte skicka ping till " + con.usr.username + ", men tuben var inte ansluten", Stuff.debuglevel.error);
                        continue;
                    }
                    con.tube.raw_send("COMPing\n");
                    Stuff.debug("Pingar " + con.usr.username + " (" + con.tube.remoteIP + ")", Stuff.debuglevel.dirty_debugging);
                    //Thread.Sleep(500);
                }
            }
            // End of main loop
        }
    }
}
