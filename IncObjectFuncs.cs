using System;
using System.Collections.Generic;
using System.Text;
using NetworkTube;
using Projekt_server;

namespace Projekt_server
{
    public partial class Program
    {
        // # The shits
        static void Inc_HiThere(HiThere hi, NTube tube)
        {
            if (!(hi.user.username.Length > 0))
            {
                Stuff.send_YouShallNotPass(tube, "Inget nick", "Du m�ste ha ett nickname f�r att ansluta");
                tube.disconnect();
                return;
            }
            Stuff.say("[Con] (" + tube.remoteIP + ") s�ger sig vara " + hi.user.username);
            // Check if username is taken
            if (Stuff.nickExists(hi.user.username))
            {
                // Sorry man, the nick's taken
                Stuff.say("Nicket upptaget!");
                Stuff.send_YouShallNotPass(tube, "Nicket upptaget", "Nicket \"" + hi.user.username + "\" anv�nds av en annan. �ndra det och f�rs�k igen");
                //ConnMgr._Remove(ConnMgr._GetCon(tube));
                tube.disconnect();
                return;
            }
            // Add the connection to the list
            User usr = hi.user;
            UserMgr._Add(usr);
            Connection nCon = new Connection();
            nCon.usr = usr;
            nCon.tube = tube;
            ConnMgr._Add(nCon);
            Stuff.say("V�lkommen.. \"" + usr.username + "\"!");
            Stuff.send_Welcome(tube, "V�lkommen till chatten " + hi.user.username);
        }


        static void Inc_Control(Control contr, NTube tube)
        {
            Connection con = ConnMgr._GetCon(tube);
            if (con == null)
                Stuff.say("[Error] Ett fel har uppst�tt! con �r null.. fjkdla!");


            switch (contr.control_type)
            {
                case "Nickchange":
                    // Nick taken?
                    if (Stuff.nickExists(contr.args[0]))
                        Stuff.send_YouShallNotPass(tube, "Nicket �r upptaget", "Nicket du f�rs�kte �ndra till (" + contr.args[0] + ") �r upptaget");
                    else
                    {
                        // Change nick
                        string oldNick = con.usr.username;
                        string newNick = contr.args[0];
                        con.usr.username = newNick;
                        Stuff.say("Nickbyte: " + oldNick + " --> " + newNick);
                        // # Broadcast to all the users in the users channels that this nick has changed (to the user too)
                        Control nChange = new Control();
                        nChange.control_type = "Nickchange";
                        nChange.args.Add(oldNick);
                        nChange.args.Add(newNick);
                        // Get all the users of the channels the nickchanging user is in and
                        // send the nickchange to all users
                        foreach (User u in Stuff.GetAllUsersInTheSameChannelsAs(con.usr))
                            ConnMgr._GetCon(u).tube.send_object<Control>(nChange);
                    }
                    break;

                case "Join":
                    // Control -> Join .. User joins a channel
                    string j_channel = contr.args[0];

                    Stuff.say("[Join] " + con.usr.username + " joinar kanalen " + j_channel);

                    foreach (Channel c in ChannelMgr._GetChanList(con.usr))
                    {
                        if (c.chan_name == j_channel)
                        {
                            Stuff.send_YouShallNotPass(tube, "Redan i kanalen", "Du �r redan i " + j_channel);
                            Stuff.say(".. var visst redan i den!");
                            return;
                        }
                    }

                    if (!ChannelMgr._Exists(j_channel))
                    {
                        Stuff.say("Skapar kanalen " + j_channel);
                        ChannelMgr._CreateChan(j_channel);
                    }

                    Channel ch = ChannelMgr._GetChan(j_channel);
                    ch.usrList.Add(con.usr);
                    
                    Control join = new Control();
                    join.control_type = "Join";
                    join.args.Add(j_channel);
                    join.args.Add(con.usr.username);
                    join.args.Add(con.usr.full_name);
                    join.args.Add(con.usr.email);
                    foreach (User u in ch.usrList)
                        ConnMgr._GetCon(u).tube.send_object<Control>(join);

                    // Send the channels userlist to the user
                    Control usrList = new Control();
                    usrList.control_type = "UserList";
                    usrList.args.Add(j_channel);
                    foreach (User u in ch.usrList)
                        usrList.args.Add(u.username);
                    tube.send_object<Control>(usrList);
                    break;

                case "Part":
                    // Control -> Part .. User parts a channel
                    string p_channel = contr.args[0];
                    bool br = true;
                    foreach (Channel c in ChannelMgr._GetChanList(con.usr))
                    {
                        if (c.chan_name == p_channel)
                        {
                            br = false;
                            break;
                        }
                    }
                    if (br)
                    {
                        Stuff.send_YouShallNotPass(tube, "Inte i kanalen", "Du �r inte i kanalen " + p_channel);
                        break;
                    }

                    Control part = new Control();
                    part.control_type = "Part";
                    part.args.Add(p_channel);
                    part.args.Add(con.usr.username);
                    foreach (User u in Stuff.GetAllUsersInTheSameChannelsAs(con.usr))
                        ConnMgr._GetCon(u).tube.send_object<Control>(part);
                    break;
            }
        }


        static void Inc_Message(Message msg, NTube tube)
        {
            User from = ConnMgr._GetCon(tube).usr;
            string mess = msg.msg;

            Stuff.say("[Msg (" + from.username + " -> " + msg.target + ") " + mess);

            Message sendMsg = new Message();
            if (msg.msg_type == "Msg") sendMsg.msg_type = "Msg"; else sendMsg.msg_type = "Me";
            sendMsg.msg = mess;
            sendMsg.from = from.username;
            sendMsg.target = msg.target;
            sendMsg.font = msg.font;
            
            // Skicka det till alla i kanalen eller till personen  (#apa resp. apa)
            if (msg.target[0] == '#')
            {
                // Target is channel
                Channel chan = ChannelMgr._GetChan(msg.target);
                List<User> sendToEm = chan.usrList;
                foreach (User u in sendToEm)
                    ConnMgr._GetCon(u).tube.send_object<Message>(sendMsg);
                // Done
            }
            else
            {
                // Target is user
                ConnMgr._GetCon(UserMgr._Search(msg.target)).tube.send_object<Message>(sendMsg);
            }
        }
    }
}
